﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace notes.data
{
    public interface INotesData
    {
        IEnumerable<string> GetAll(); 
    }

    public class InMemoryData : INotesData 
    {
        public IEnumerable<string> GetAll()
        {
            List<string> notes = new List<string>() 
            {
                "hello",
                "Rosetta Stone"
            };
            return notes;
        }
    }
}
